# LudicApp

Project developed in ReactJS and Firebase. This project is a simple social network where players can hang out and share board and cards games.

---

Questo progetto effettuato con il fondo Bando alle ciance propone un esempio di Social Network a costo zero, fatto con react per la parte frontend, firebase per la parte backend utilizzando google cloud per la parte di hosting

[Link al progetto finale](https://ludicapp-db.web.app)

## Comandi utili

Per eseguire il progetto lanciare nella cartella del programma 
```
npm start
```
Per aggiornare il sito firebase di hosting
```
npm run build;
firebase deploy
```

## Link utili
- [Link a guida Firebase Hosting](https://medium.com/swlh/how-to-deploy-a-react-app-with-firebase-hosting-98063c5bf425)
- [Link a guida creazione sito React + Firebase](https://www.youtube.com/watch?v=zL0dKETbCNE&list=PLDHWK63BVywzc_uN3RnQq-z_dg7VJbokv&index=2&t=3165s)

# Screenshots

![Layers](readme_screenshot_1.png)

![Layers](readme_screenshot_2.png)

![Layers](readme_screenshot_3.png)

## Chi siamo
Developed by Carlo Tacchella

---

Sviluppo software a cura di Carlo Tacchella.

## Data di creazione
22/09/07
