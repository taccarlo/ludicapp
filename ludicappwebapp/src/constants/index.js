
import {COLORS, SIZES, FONTS} from './theme';
import {CONFIGURATION} from './configuration';

export {COLORS, SIZES, FONTS, CONFIGURATION};
