
import {addDoc, updateDoc, deleteDoc, doc, collection, getDocs} from 'firebase/firestore';
import {db} from "./firebase-config"
    
    const postsCollectionRef = collection(db, "posts");
    
    const createDocInCollection = async (par, collectionName) => {
        const collectionRef = collection(db, collectionName);
        await addDoc(
            collectionRef, 
            par
        );
    };

    const changeDocInCollection = async (par, collectionName, id) => {  
       const postRef = doc(db, collectionName, id);
        await updateDoc(postRef, par);
    };

    const deleteDocInCollection = async (par, collectionName) => {
        const postDoc = doc(db, collectionName, par);  
        await deleteDoc(postDoc);
    };

    const createDocPosts = async (par) => {
        await addDoc(
            postsCollectionRef, 
            par
        );
    };

    const deleteDocPosts = async (par) => {       
        const postDoc = doc(db, "posts", par);
        await deleteDoc(postDoc);
    };

    const getDocsPosts = async() => {
        const data = await getDocs(collection(db, "posts"));
        return data;
    }

    const getComments = async(id) => {
        const data = await getDocs(collection(db, "comments/"+id+"/feed"));
        return data;
    }

    export {
        createDocPosts,
        deleteDocPosts,
        getDocsPosts,
        getComments,
        createDocInCollection,
        changeDocInCollection,
        deleteDocInCollection
    };
