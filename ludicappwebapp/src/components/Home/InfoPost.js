import React, {useState} from "react";
import Comments from "./Comments";
function isAdminUser(id){
  //TODO: reach this ids from server
  //ID info.ludicapp
  var ids = ["Y9hc3tzjZwWEU0YqvB9MEBzjnkZ2"];
  return ids.includes(id);

}
function InfoPost(props){

  const [titleChanged, setTitleChanged] = useState("");
  const [descriptionChanged, setDescriptionChanged] = useState("");
  const [emailChanged, setEmailChanged] = useState("");
  const [phoneChanged, setPhoneChanged] = useState("");
  const [eventDataChanged, setEventDataChanged] = useState("");
  const [gameListChanged, setGameListChanged] = useState("");

  let idUserConnected = (props.uid.currentUser === null) ? null : props.uid.currentUser.uid;
  let nameUserConnected = (props.uid.currentUser === null) ? null : props.uid.currentUser.displayName;
    return <div className = "createPostPage">

      <div className="normal-page">
        <div className="container page">
          <div className="row">
            <div className="col-md-10 offset-md-1 col-xs-12">

                <div className="article-preview">
                  <div className="preview-link">
                  {(!(props.data.author.id_user === idUserConnected && idUserConnected !== null)  &&
                    <div>
                    <h1>{props.data.title.toUpperCase()}</h1>
                    <h4>{props.data.description}</h4>
                    <h6>{props.data.nameComplete}</h6>
                    <h6>{props.data.address}</h6>


                    <h6>{(props.uid.currentUser === null) ? "Esegui il login per vedere le informazioni per contattare l'autore del post.":props.data.email}</h6>
                    <h6>{(props.uid.currentUser === null) ? "":props.data.phone}</h6>

                    <h6>{props.data.event.eventData}</h6>
                    <h6>{props.data.game.gameList}</h6>
                    </div>
                  )}
                   {((props.data.author.id_user === idUserConnected || isAdminUser(idUserConnected)) && idUserConnected !== null  &&
                    <div>
                    <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder={props.data.title===""?"Inserisci il titolo":props.data.title}
                      onChange={
                          (event) => {
                              setTitleChanged(event.target.value);
                          }
                      }
                    />
                    </fieldset>
                    <fieldset className="form-group">
                    <textarea
                      rows={5}
                      className="form-control"
                      type="text"
                      placeholder={props.data.description===""?"Inserisci la descrizione":props.data.description}
                      onChange={
                          (event) => {
                              setDescriptionChanged(event.target.value);
                          }
                      }
                    />
                    </fieldset>
                    <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder={props.data.email===""?"Inserisci la email":props.data.email}
                      onChange={
                          (event) => {
                              setEmailChanged(event.target.value);
                          }
                      }
                    />
                    </fieldset>
                    <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder={props.data.phone===""?"Inserisci il tuo numero":props.data.phone}
                      onChange={
                          (event) => {
                              setPhoneChanged(event.target.value);
                          }
                      }
                    />
                    </fieldset>
                    {(props.data.type==="game")
                    &&
                    <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder={(props.data.game.gameList===undefined || props.data.game.gameList==="")?"Inserisci la lista dei giochi":props.data.game.gameList}
                      onChange={
                          (event) => {
                              setGameListChanged(event.target.value);
                          }
                      }
                    />
                    </fieldset>
                    }
                    {(props.data.type==="event")
                    &&
                    <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder={(props.data.event.eventData===undefined || props.data.event.eventData==="")?"Inserisci la data dell'evento":props.data.event.eventData}
                      onChange={
                          (event) => {
                              setEventDataChanged(event.target.value);
                          }
                      }
                    />
                    </fieldset>
                    }
                    <button  className="btn btn-danger"
                          onClick={() => {
                            props.callbackDelete(props.data.id);
                          }}
                        >Cancella
                    </button>

                    <button
                    className="btn btn-primary pull-xs-right"
                    onClick={()=>{props.callbackUpdate(props.data.id, {
                      title:titleChanged,
                      description:descriptionChanged,
                      phone:phoneChanged,
                      email:emailChanged,
                      gameList:gameListChanged,
                      eventData:eventDataChanged
                    })}}
                    type="submit">
                      Modifica
                    </button>
                    </div>
                    )}
                  </div>


                  <Comments id = {props.data.id} userId = {idUserConnected} userName ={nameUserConnected} authorId={props.data.author.id_user}></Comments>

                  </div>

            </div>
          </div>
        </div>
      </div>
    </div>
}
export default InfoPost;



