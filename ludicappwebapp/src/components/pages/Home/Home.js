import React, { useRef, useEffect, useState } from "react";
import MapView from '../../Home/MapView';
import { changeDocInCollection, deleteDocPosts, getDocsPosts } from "../../../db/db-mng";
import InfoPost from "../../Home/InfoPost";
import sponsor from '../../../assets/img/sponsor.png';
import { auth } from "../../../db/firebase-config";

function Home() {

  const [posts, setPosts] = useState([]);
  const [postFocused, setPostFocused] = useState(null);
  const postInfoPoint = useRef(null);

  const getPosts = async () => {

    const data = await getDocsPosts();
    const temp = data.docs.map((doc) => ({ ...doc.data(), id: doc.id }))
    setPosts(temp);
  };

  useEffect(() => {
    getPosts();
  }, []);

  const deletePost = async (id) => {
    const confirmBox = window.confirm(
      "Vuoi Davvero eliminare il commento?"
    )
    if (confirmBox === true) {
      await deleteDocPosts(id);
      getPosts();
      alert("Post cancellato");
      setPostFocused(null);
    }
  };

  const updatePost = async (id, par) => {
    
    let payload;
    
    if(par.gameList!=="" && par.eventData!==""){
      payload = {event:{}, game:{}};
    }
    else if( par.eventData!==""){
      payload = {event:{}};
    }
    else if( par.gameList!==""){
      payload = {game:{}};
    }
    else{
      payload = {};
    }

    if(par.title!==""){
      payload["title"]=par.title;
    }
    if(par.description!==""){
      payload["description"]=par.description;
    }
    if(par.phone!==""){
      payload["phone"]=par.phone;
    }
    if(par.email!==""){
      payload["email"]=par.email;
    }
    if(par.gameList!==""){
      payload.game.gameList=par.gameList;
    }
    if(par.eventData!==""){
      payload.event.eventData=par.eventData;
    }

    await changeDocInCollection(
        payload,
        "posts",
        postFocused.id
    );

    getPosts();

    setPostFocused(null);
  };

  const callbackMap = (index) => {
    setPostFocused(posts[index]);
    
    //getComments and pass to InfoPostReadData
    postInfoPoint.current.scrollIntoView({
      behavior: "smooth",
    });

  }

  return <div>

    <MapView data={posts} callback={callbackMap}></MapView>

    <img src={sponsor} className="all-width-img" alt="I nostri sponsor" />

    <div className="homePage" ref={postInfoPoint}>

      {
        postFocused != null && (
            <InfoPost data={postFocused} uid={auth} callbackDelete={deletePost} callbackUpdate={updatePost} />
        )
      }
    </div>
  </div>;

}
export default Home;