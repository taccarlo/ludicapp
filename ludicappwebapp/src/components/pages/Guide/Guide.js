import React from "react";
import Banner from "../../../components/Team/Banner";
import icon from '../../../assets/img/icon.png';
import { useNavigate } from "react-router-dom";

function Guide() {

  let navigate = useNavigate();

  const goToHome = async () => {
    //go to guide page
    navigate("/");
  }
    return <div className="guidePage">
        <div className="normal-page">
            <div className="container page">
                <Banner></Banner>
                <h4><b>Entra nella community di LudicApp!</b></h4>
                <h4><b>REGOLAMENTO</b></h4>

                <div className="banner">
                    <div className="container">
                        <p>
                            In questa piattaforma non esistono delle regole di comportamento definite, perché i bisogni di ogni giocatore sono specifici, sia di chi offre il prestito sia di chi lo richiede. Quindi noi ci sentiamo di dare dei criteri che siano utili a creare un buon rapporto di fiducia tra i vari giocatori durante lo scambio. Cercheremo di dare più attenzione alla persona che si offre di prestare, visto che è quella più vulnerabile e quella con più esigenze di sicurezza.
                        </p>
                    </div>
                </div>
                <h4><b>CONTATTARSI</b></h4>

                <div className="banner">
                    <div className="container">
                        <p>
                            Nell'icona della persona ci sono i contatti che lui ritiene più sicuri e di cui ha fiducia. Quindi vi consigliamo di seguire il metodo scelto da chi offre il gioco.<br />
                            (es. se sceglie di dare una email e non il cellulare, incoraggiamo di usare quel metodo e non di non insistere per richiedere il numero privato)
                        </p>
                    </div>
                </div>

                <h4><b>RITIRO</b></h4>

                <div className="banner">
                    <div className="container">
                        <p>
                            Specificate se avete dei limiti di spostamento, come l'assenza di macchina o l'essere in una determinata zona solo in momenti specifici. In alternativa, cercate di venire incontro alle esigenze di chi offre.<br/>Ad esempio: chi offre potrebbe venire dove abitate a recapitare il gioco, proporvi di trovarsi in un luogo neutro o farvi andare a casa sua a recuperarlo.
                        </p>
                    </div>
                </div>
                <h4><b>DURATA</b></h4>

                <div className="banner">
                    <div className="container">
                        <p>
                            Non sempre il giorno del ritiro coincide con il giorno in cui si prova il gioco prestato. Quindi vi consigliamo di avere già una data possibile per giocarci e concordare l'effettiva durata del prestito. Con un referendum tra giocatori abbiamo percepito che il prestito potrebbe variare da 7 a 20 giorni. Dipende sempre dalla volontà e le esigenze di chi mette in comune.
                        </p>
                    </div>
                </div>

                <h4><b>ACCORDO FINALE</b></h4>

                <div className="banner">
                    <div className="container">
                        <p>
                            Cercate di avere un messaggio scritto con gli accordi che avete preso in dettaglio.<br />
                            ( Esempio: Io verrò a casa tua a ritirare il gioco “I COLONI DI CATAN”  il 6 giugno alle 20:00, e il 16 giugno te lo consegnerò a casa entro le 18.00 )
                        </p>
                    </div>
                </div>
                <h4><b>COMMENTO</b></h4>

                <div className="banner">
                    <div className="container">
                        <p>
                            Alla fine dello scambio vi consigliamo di lasciare un commento sullo scambio, scrivendo se sono stati rispettati gli ACCORDI FINALI.
                            Suggeriamo di aggiungere una recensione sull'affidabilità di chi ha ricevuto il prestito da parte di chi lo ha offerto, per rendere la persona meritevole di fiducia per altri prestiti.
                            Non suggeriamo di commentare lo stato del gioco, se avvisati prima dalla persona o se non è influente per la giocata.
                            (esempio: non suggeriamo di scrivere che la scatola è rovinata dall'esterno).
                        </p>
                    </div>
                </div>

                <h5>
            <button
                onClick = {() => {
                    goToHome();
                  }}
                className="btn btn-sm pull-xs-right btn-primary"
            >
                <img src={icon} style={{height: 40, marginRight:10}} alt = ""/>
                <b>Buon gioco! </b> 
            </button>
            </h5>
            </div>
        </div>
    </div>;
}
export default Guide;